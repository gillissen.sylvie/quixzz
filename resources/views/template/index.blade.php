{{--
./resources/views/template/index.blade.php
--}}

<!DOCTYPE html>
<html lang="en">
    @include('template.partials._head')
<body>
    <h1 class="text-center bg-red-500">Salut Quixzz</h1>

    <div x-data="{ count: 0 }">
        <button @click="count++">Increment</button>
        <span x-text="count"></span>
    </div>

    <div x-data="{ open: false }">
        <button @click="open = ! open">Toggle</button>
        <div x-show="open" @click.outside="open = false">Contents...</div>
    </div>

</body>
</html>